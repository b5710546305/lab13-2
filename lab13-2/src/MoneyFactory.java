import java.util.ResourceBundle;
/**
 * The super class of classes that produces money,
 * It's Singleton
 * @author Parinvut Rochanavedya
 */
public abstract class MoneyFactory{
	private static MoneyFactory instance;
	private static int type = 0;
	/**
	 * get the singleton instance
	 * @return this thing
	 */
	public static MoneyFactory getInstance(){
		if(instance == null){
			if(type == 1){
				instance = new ThaiMoneyFactory();
			} else {
				instance = new MalaiMoneyFactory();
			}
		}
		return instance;
	}
	/**
	 * create the instance by reading the file
	 * @return
	 */
	static MoneyFactory BundleInstance(){
		if(instance == null){
			ResourceBundle bundle = ResourceBundle.getBundle("purse");
			String factoryclass = bundle.getString("moneyfactory");
			
			try{
				instance = (MoneyFactory)Class.forName(factoryclass).newInstance();
			}catch(Exception e){
				System.err.println("Error creating MoneyFactory "+e.getMessage());
				e.printStackTrace();
			}
		}
		return instance;
	}
	/**
	 * Method for creating money objects for sub-classes
	 */
	abstract Valuable  createMoney(double value);
	
}
