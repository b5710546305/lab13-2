/**
 * A malaysian coin with a monetary value.
 * You can't change the value of a coin.
 * @author  Parinvut Rochanavedya 
 */
public class SenCoin implements Valuable{
	
	 private double value;

    /** 
     * Constructor for a new coin. 
     * @param value is the value for the coin
     */
    public SenCoin( double value ) {
        this.value = value;
    }

    
    /**
     * Print out the coin and it's value
     */
    public String toString(){
    	return String.format("%.0f",value) + " Sen Coin";
    }


	@Override
	public int compareTo(Valuable o) {
		// TODO Auto-generated method stub
		return (int) (this.getValue() - o.getValue());
	}


	@Override
	public double getValue() {
		// TODO Auto-generated method stub
		return value;
	}
    
    
}
