
 
/**
 * A coin with a monetary value.
 * You can't change the value of a coin.
 * @author  Parinvut Rochanavedya 
 * @version 2015.02.10
 */
public class Coin implements Valuable{
	
	 private double value;

    /** 
     * Constructor for a new coin. 
     * @param value is the value for the coin
     */
    public Coin( double value ) {
        this.value = value;
    }

    
    /**
     * Print out the coin and it's value
     */
    public String toString(){
    	return String.format("%.0f",value) + " Baht Coin";
    }


	@Override
	public int compareTo(Valuable o) {
		// TODO Auto-generated method stub
		return (int) (this.getValue() - o.getValue());
	}


	@Override
	public double getValue() {
		// TODO Auto-generated method stub
		return value;
	}
    
    
}
