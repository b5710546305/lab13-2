/**
 * A bank note with a monetary value.
 * You can't change the value of a bank note.
 * @author  Parinvut Rochanavedya 
 */
public class BankNote implements Valuable{

    /** Value of the bank note */
    private static long nextSerialNumber = 1000000;
    private long serialNumber;
    private double value;
    
    /** 
     * Constructor for a new bank note. 
     * @param value is the value for the bank note
     */
    public BankNote( double value ) {
        this.value = value;
        serialNumber = getNextSerialNumber();
    }
    
    /**
     * Return and update the serial number of bank notes
     * @return the next serial number from current
     */
    public static long getNextSerialNumber(){
    	return nextSerialNumber++;
    }
    
    /**
     * Return and the serial number of bank note
     * @return the serial number
     */
    public long getSerialNumber(){
    	return serialNumber;
    }

    /**
     * Print out the bank note and it's value
     */
    public String toString(){
    	return String.format("%.0f",value) + " Baht BankNote";
    }

	@Override
	public int compareTo(Valuable arg0) {
		// TODO Auto-generated method stub
		return (int) (this.getValue() - arg0.getValue());
	}

	@Override
	public double getValue() {
		// TODO Auto-generated method stub
		return value;
	}

    
    
}
