/**
 * Malaysia Money Factory
 * @author Parinvut Rochanavedya
 */
public class MalaiMoneyFactory extends MoneyFactory{

	@Override
	Valuable createMoney(double value) {
		if(value >= 0.05 && value < 1){
			return new SenCoin(value);
		} else if(value >= 1 && value <= 100){
			return new RinggitBankNote(value);
		}
		return null;
	}

}