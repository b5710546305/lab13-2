
/**
 * An interface for a monetary value objects.
 * @author  Parinvut Rochanavedya 
 * @version 2015.02.10
 */
public interface Valuable extends Comparable<Valuable>{
		/**
	 	* Value of the object which is monetary.
	 	* @return the value
	 	*/
		public double getValue( );
}