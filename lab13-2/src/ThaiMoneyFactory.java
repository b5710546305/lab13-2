/**
 * Thai Money Factory
 * @author Parinvut Rochanavedya
 */
public class ThaiMoneyFactory extends MoneyFactory{

	@Override
	Valuable createMoney(double value) {
		if(value > 0 && value < 20){
			return new Coin(value);
		} else if(value > 20 && value <= 1000){
			return new BankNote(value);
		}
		return null;
	}

}
